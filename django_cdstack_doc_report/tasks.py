import tempfile
from io import BytesIO

from celery import shared_task
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import registerFontFamily, registerFont
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Paragraph, LongTable

from django_cdstack_models.django_cdstack_models.models import *


def optimize_text(string):
    if string is None:
        string = ""

    string = string.replace("\r\n", "<br />")
    string = string.replace("\n", "<br />")

    if len(string) > 1000:
        string = string[0:1000] + "<br />[...]"

    return string


@shared_task(name="cdstack_doc_report_create")
def cdstack_doc_report_create(instance_id: int, recipient_email: str):
    instance = CmdbInstance.objects.get(id=instance_id)
    groups = CmdbGroup.objects.filter(inst_rel=instance)
    hosts = CmdbHost.objects.filter(inst_rel=instance)

    # Styles
    main_style = getSampleStyleSheet()
    registerFont(TTFont("verdana", "verdana.ttf"))
    registerFontFamily("verdana", normal="verdana", bold="verdana", italic="verdana")

    pdf_file = BytesIO()
    pdf_file = tempfile.TemporaryFile(delete=False)

    doc = SimpleDocTemplate(
        pdf_file,
        pagesize=A4,
        leftMargin=1 * cm,
        rightMargin=1 * cm,
        topMargin=1 * cm,
        bottomMargin=1 * cm,
    )

    # container for the 'Flowable' objects
    elements = []

    title = Paragraph("Groups", main_style["Heading1"])
    elements.append(title)

    for group in groups:
        group_name = Paragraph(group.name, main_style["Heading2"])
        elements.append(group_name)

        data = []

        table_header = ["Variable", "Value"]
        data.append(table_header)

        for variable in group.cmdbvarsgroup_set.all():
            table_row = [
                variable.name,
                Paragraph(optimize_text(variable.value), main_style["BodyText"]),
            ]
            data.append(table_row)

        t = LongTable(data, repeatRows=1, colWidths="50%")
        elements.append(t)

    title = Paragraph("Hosts", main_style["Heading1"])
    elements.append(title)

    for host in hosts:
        group_name = Paragraph(host.hostname, main_style["Heading2"])
        elements.append(group_name)

        data = []

        table_header = ["Variable", "Value"]
        data.append(table_header)

        for variable in host.cmdbvarshost_set.exclude(
            name="info_dump_cpuz_report_html"
        ).all():
            table_row = [
                variable.name,
                Paragraph(optimize_text(variable.value), main_style["BodyText"]),
            ]
            data.append(table_row)

        t = LongTable(data, repeatRows=1, colWidths="50%")
        elements.append(t)

        try:
            cpuz = host.cmdbvarshost_set.get(name="info_dump_cpuz_report_html")
        except CmdbVarsHost.DoesNotExist:
            pass
        else:
            cpuz_report_title = Paragraph("Hardware", main_style["Heading1"])
            elements.append(cpuz_report_title)

            # cpuz_report = Paragraph(cpuz.value, main_style['Paragraph'])
            # elements.append(cpuz_report)

    # write the document to disk
    doc.build(elements)

    pdf_file.close()
    del pdf_file

    return
